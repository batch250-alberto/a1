package com.zuitt.example;

import java.util.Scanner;
public class PersonInput {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = myObj.nextLine();

        System.out.println("Last Name:");
        String lastName = myObj.nextLine();

        System.out.println("First Subject Grade: ");
        double num1 = myObj.nextInt();
        System.out.println("Second Subject Grade: ");
        double num2 = myObj.nextInt();
        System.out.println("Third Subject Grade: ");
        double num3 = myObj.nextInt();

        double average = (num1 + num2 + num3)/3;
        System.out.println("Good Day, " + firstName + " " + lastName);
        System.out.println("Your average grade is: " + Math.round(average));
    }

}
